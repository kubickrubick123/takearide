INSERT INTO category (id, name, url, product_count)
VALUES (1, 'SUV', '/suv', 1);

INSERT INTO category (id, name, url, product_count)
VALUES (3, 'econom', '/econom', 3);

INSERT INTO category (id, name, url, product_count)
VALUES (4, 'business', '/business', 1);

INSERT INTO category (id, name, url, product_count)
VALUES (5, 'sport', '/sport', 9);

INSERT INTO category (id, name, url, product_count)
VALUES (6, 'truck', '/truck', 2);



INSERT INTO producer (id, name, product_count)
VALUES (1, 'BMW', 13);

INSERT INTO producer (id, name, product_count)
VALUES (2, 'Porsche', 2);

INSERT INTO producer (id, name, product_count)
VALUES (3, 'LandRover', 2);

INSERT INTO producer (id, name, product_count)
VALUES (4, 'Daewoo', 1);

INSERT INTO producer (id, name, product_count)
VALUES (5, 'Mercedes-Benz', 1);

INSERT INTO producer (id, name, product_count)
VALUES (6, 'Volkswagen', 1);




INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (3, 'BMW M5', 'the Greatest CAR in the world', 'media/iphone8.jpg', 666.00, 5, 1);

INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (4, 'RangeRover Defender', 'Proedet VEzde.Blya bydy', 'media/gear.jpg', 300.00, 1, 3);

INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (1, 'Daewoo Lanos', 'хуйня полная', 'media/iphone7.jpg', 1.50, 3, 1);

INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (2, 'Panamera 4s', 'для пижонов', 'media/iphone6s.jpg', 399.00, 4, 2);

INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (5, 'VW Transporter', 'Transporter', 'media/vwt.jpg', 100.00, 6, 6);

INSERT INTO product (id, name, description, image_link, price, id_category, id_producer)
VALUES (7, 'Mercedes-Benz Sprinter', 'Sprinter', 'media/sprinter.jpg', 120.00, 6, 5);



INSERT INTO account (id, name, email, lastname, login, "isEmailNotificationsEnabled", "imageLink", password)
VALUES (2, 'Oleksandr', 'kubickrubick123@gmail.com', 'Onopriienko', 'kubick', false, '015810123ae8685e870086cc4d68bab9.jpg', '123456');






ALTER TABLE ONLY category
    ADD CONSTRAINT category_pkey
    PRIMARY KEY (id);
--
-- Definition for index category_url_key (OID = 16403) :
--
ALTER TABLE ONLY category
    ADD CONSTRAINT category_url_key
    UNIQUE (url);
--
-- Definition for index producer_pkey (OID = 16409) :
--
ALTER TABLE ONLY producer
    ADD CONSTRAINT producer_pkey
    PRIMARY KEY (id);
--
-- Definition for index product_pkey (OID = 16417) :
--
ALTER TABLE ONLY product
    ADD CONSTRAINT product_pkey
    PRIMARY KEY (id);
--
-- Definition for index product_fk (OID = 16419) :
--
ALTER TABLE ONLY product
    ADD CONSTRAINT product_fk
    FOREIGN KEY (id_category) REFERENCES category(id) ON UPDATE CASCADE;
--
-- Definition for index product_fk1 (OID = 16424) :
--
ALTER TABLE ONLY product
    ADD CONSTRAINT product_fk1
    FOREIGN KEY (id_producer) REFERENCES producer(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index account_pkey (OID = 16432) :
--
ALTER TABLE ONLY account
    ADD CONSTRAINT account_pkey
    PRIMARY KEY (id);
--
-- Definition for index account_email_key (OID = 16434) :
--
ALTER TABLE ONLY account
    ADD CONSTRAINT account_email_key
    UNIQUE (email);
--
-- Definition for index order_pkey (OID = 16440) :
--
ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_pkey
    PRIMARY KEY (id);
--
-- Definition for index order_fk (OID = 16442) :
--
ALTER TABLE ONLY "order"
    ADD CONSTRAINT order_fk
    FOREIGN KEY (id_account) REFERENCES account(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Definition for index order_item_pkey (OID = 16450) :
--
ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_pkey
    PRIMARY KEY (id);
--
-- Definition for index order_item_fk (OID = 16452) :
--
ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_fk
    FOREIGN KEY (id_order) REFERENCES "order"(id) ON UPDATE CASCADE ON DELETE CASCADE;
--
-- Definition for index order_item_fk1 (OID = 16457) :
--
ALTER TABLE ONLY order_item
    ADD CONSTRAINT order_item_fk1
    FOREIGN KEY (id_product) REFERENCES product(id) ON UPDATE CASCADE ON DELETE RESTRICT;
--
-- Data for sequence public.account_seq (OID = 16462)
--
SELECT pg_catalog.setval('account_seq', 2, true);
--
-- Data for sequence public.order_seq (OID = 16464)
--
SELECT pg_catalog.setval('order_seq', 31, true);
--
-- Data for sequence public.order_item_seq (OID = 16466)
--
SELECT pg_catalog.setval('order_item_seq', 29, true);
--
-- Comments
--
COMMENT ON SCHEMA public IS 'standard public schema';