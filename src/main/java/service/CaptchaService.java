package service;
import strategy.captchastrategy.AbstractStrategy;
import strategy.captchastrategy.AttributeCaptchaStrategy;
import strategy.captchastrategy.CookieCaptchaStrategy;
import strategy.captchastrategy.HiddenFieldCaptchaStrategy;
import service.abstractservice.AbstractService;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;

public class CaptchaService implements AbstractService {
    private final Map<String, Supplier<AbstractStrategy>> captchaStrategyMap = new LinkedHashMap<>();

    public CaptchaService() {
        initCaptchaStrategyMap();
    }

    public Map<String,Supplier<AbstractStrategy>> getCaptchaStrategyMap() {
        return captchaStrategyMap;
    }

    private void initCaptchaStrategyMap() {
        captchaStrategyMap.put("cookie",() -> new CookieCaptchaStrategy());
        captchaStrategyMap.put("attribute",() -> new AttributeCaptchaStrategy());
        captchaStrategyMap.put("hiddenField",() -> new HiddenFieldCaptchaStrategy());
    }
}
