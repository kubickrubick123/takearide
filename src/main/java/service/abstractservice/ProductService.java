package service.abstractservice;
import entity.Category;
import entity.Producer;
import entity.Product;
import form.SearchForm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ProductService extends AbstractService {
    List<Product> listAllProducts(int page, int limit, HttpServletRequest request);

    List<Product> listProductsByCategory(String categoryUrl,int page,int limit,HttpServletRequest request);

    List<Category> listAllCategories();

    List<Producer> listAllProducers();

    int countAllProducts();

    int countProductsByCategory(String categoryUrl);

    int countProductsBySearchForm(SearchForm searchForm);

    List<Product> listProductsBySearchForm(SearchForm searchForm, int page, int limit,HttpServletRequest request);
}
