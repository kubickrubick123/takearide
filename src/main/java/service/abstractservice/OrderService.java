package service.abstractservice;

import entity.Order;
import entity.User;
import form.ProductForm;
import model.ShoppingCart;

import java.sql.SQLException;
import java.util.List;

public interface OrderService extends AbstractService {

   void addProductToShoppingCart(ProductForm form, ShoppingCart shoppingCart);

   void removeProductFromShoppingCart(ProductForm productForm,ShoppingCart shoppingCart);

   String serializeShoppingCart(ShoppingCart shoppingCart);

   ShoppingCart deserializeShoppingCart(String string);

   long makeOrder(ShoppingCart shoppingCart, User user,boolean isUSerWantToPayByCash);

   List<Order> listMyOrders(User user,int page,int limit);

   Order findOrderById(long id,User user);
}
