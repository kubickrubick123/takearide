package service;
import constants.Constants;
import entity.User;
import jdbc.CRUDOperations;
import jdbc.ResultSetHandler;
import jdbc.ResultSetHandlerFactory;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import service.abstractservice.AbstractService;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;

public class UserService implements AbstractService {
    private static final ResultSetHandler<User> accountResultSetHandler =
            ResultSetHandlerFactory.getSingleResultSetHandler(ResultSetHandlerFactory.ACCOUNT_RESULT_SET_HANDLER);
    private final DataSource dataSource;

    public UserService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public boolean checkUserLogin(User user) {
        try (Connection c = dataSource.getConnection()) {
            User account = CRUDOperations.select(c, "select * from account where login=?", accountResultSetHandler, user.getLogin());
            if (account == null) {
                return true;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
         user.setUserFailedCauseMessage(Constants.LOGIN_ALREADY_IN_USE);
        return false;
    }

    public User getUser(String login, String password) {
        try (Connection c = dataSource.getConnection()) {
            User account = CRUDOperations.select(c, "select * from account where login=?", accountResultSetHandler, login);
            if (account != null && password.equals(account.getPassword())) {
                return account;
            }
        } catch (SQLException e) {
            System.err.println(e.getMessage());
        }
        return null;
    }

    public User buildUserFromRequest(HttpServletRequest req) throws IOException, ServletException {
        Part filePart = req.getPart("avatar");
        String fileName = getSubmittedFileName(filePart);
        saveImage(filePart, fileName, req);
        return new User(req.getParameter("username"), req.getParameter("login"), req.getParameter("lastname"), req.getParameter("password1"),
                req.getParameter("email"), Boolean.parseBoolean(req.getParameter("keeppost")), fileName);
    }


    public void saveUserToDataBase(User user) {
        try(Connection c = dataSource.getConnection()) {
            CRUDOperations.insert(c, "insert into account values (nextval('account_seq'),?,?,?,?,?,?,?,?)", accountResultSetHandler, user.getFirstName(), user.getEmail(),
                    user.getLastName(),user.getLogin(),user.isEmailNotificationsAccepted(),user.getImageLink(),user.getPassword(),Constants.DEFAULT_USER_ROLE);
            c.commit();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private String getSubmittedFileName(Part part) {
        for (String cd : part.getHeader("content-disposition").split(";")) {
            if (cd.trim().startsWith("filename")) {
                String fileName = cd.substring(cd.indexOf('=') + 1).trim().replace("\"", "");
                return fileName.substring(fileName.lastIndexOf('/') + 1).substring(fileName.lastIndexOf('\\') + 1);
            }
        }
        return null;
    }

    private void saveImage(Part filePart, String fileName, HttpServletRequest req) throws
            IOException{
        InputStream fileContent = filePart.getInputStream();
        OutputStream outputStream = new FileOutputStream("\\static\\avatar\\" + fileName);
        IOUtils.copy(fileContent, outputStream);
        outputStream.close();
    }

    public boolean isUserFieldsNotNull(User user) {
        return user.getLogin() != null && user.getEmail() != null && user.getPassword() != null && user.getFirstName() != null && user.getLastName() != null;
    }
}
