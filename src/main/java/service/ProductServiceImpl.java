package service;
import entity.Category;
import entity.Producer;
import entity.Product;
import exception.InternalServerErrorException;
import form.SearchForm;
import jdbc.CRUDOperations;
import jdbc.ResultSetHandler;
import jdbc.ResultSetHandlerFactory;
import jdbc.SearchQuery;
import producFilter.ProductPriceFilter;
import service.abstractservice.ProductService;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ProductServiceImpl implements ProductService {
    private static final ResultSetHandler<List<Product>> productsResultSetHandler =
            ResultSetHandlerFactory.getListResultSetHandler(ResultSetHandlerFactory.PRODUCT_RESULT_SET_HANDLER);

    private final ResultSetHandler<List<Category>> categoryListResultSetHandler =
            ResultSetHandlerFactory.getListResultSetHandler(ResultSetHandlerFactory.CATEGORY_RESULT_SET_HANDLER);

    private final ResultSetHandler<List<Producer>> producerResultSetHandler =
            ResultSetHandlerFactory.getListResultSetHandler(ResultSetHandlerFactory.PRODUCER_RESULT_SET_HANDLER);

    private static final ResultSetHandler<Integer> countResultSetHandler = ResultSetHandlerFactory.getCountResultSetHandler();

    private final DataSource dataSource;

    public ProductServiceImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }
    @Override
    public List<Product> listAllProducts(int page, int limit,HttpServletRequest request) {
        try(Connection c = dataSource.getConnection()){
            int offset = (page -1) * limit;
            List<Product> listAllProducts = CRUDOperations.select(c,"select p.*, c.name as category,pr.name as producer from product p, producer pr,category c where c.id=p.id_category and pr.id=p.id_producer limit ? offset ?",productsResultSetHandler,limit,offset);
            ProductPriceFilter productPriceFilter = (ProductPriceFilter) request.getSession().getAttribute("priceSortFilter");
            if(productPriceFilter != null) {
                return productPriceFilter.sortProductListByPrice(listAllProducts);
            }
            return listAllProducts;
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage());
        }
    }

    @Override
    public List<Product> listProductsByCategory(String categoryUrl, int page, int limit,HttpServletRequest request) {
        try (Connection c = dataSource.getConnection()) {
            int offset = (page - 1) * limit;
            List<Product>  listProductsByCategory =  CRUDOperations.select(c,
                    "select p.*, c.name as category, pr.name as producer from product p, category c, producer pr where c.url=? and pr.id=p.id_producer and c.id=p.id_category order by p.id limit ? offset ?",
                    productsResultSetHandler, categoryUrl, limit, offset);
            ProductPriceFilter productPriceFilter = (ProductPriceFilter) request.getSession().getAttribute("priceSortFilter");
            if(productPriceFilter != null) {
                return productPriceFilter.sortProductListByPrice(listProductsByCategory);
            }
            return listProductsByCategory;
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage(), e);
        }
    }

    @Override
    public List<Category> listAllCategories() {
        try (Connection c = dataSource.getConnection()) {
            return CRUDOperations.select(c, "select * from category order by id", categoryListResultSetHandler);
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage(), e);
        }
    }

    @Override
    public List<Producer> listAllProducers() {
        try(Connection connection = dataSource.getConnection()) {
            return CRUDOperations.select(connection,"select * from producer order by id",producerResultSetHandler);
        } catch (SQLException e) {
            throw new InternalServerErrorException("Cant execute sql query" + e.getMessage(), e);
        }
    }

    @Override
    public int countAllProducts() {
        try(Connection c = dataSource.getConnection()){
            return CRUDOperations.select(c,"select count(*) from product",countResultSetHandler);
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage());
        }
    }

    @Override
    public int countProductsByCategory(String categoryUrl) {
        try (Connection c = dataSource.getConnection()) {
            return CRUDOperations.select(c, "select count(p.*) from product p, category c where c.id=p.id_category and c.url=?", countResultSetHandler, categoryUrl);
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage(), e);
        }
    }

    @Override
    public int countProductsBySearchForm(SearchForm searchForm) {
        try (Connection c = dataSource.getConnection()) {
            SearchQuery searchQuery = buildSearchQuery("count(*)",searchForm);
            return CRUDOperations.select(c,searchQuery.getSql().toString(),countResultSetHandler,searchQuery.getParams().toArray());
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage(), e);
        }
    }


    @Override
    public List<Product> listProductsBySearchForm(SearchForm searchForm, int page, int limit,HttpServletRequest request) {
        try(Connection c = dataSource.getConnection()){
            int offset = (page -1) * limit;
            SearchQuery searchQuery = buildSearchQuery("p.*,c.name as category,pr.name as producer",searchForm);
            searchQuery.getSql().append(" order by p.id limit ? offset ?");
            searchQuery.getParams().add(limit);
            searchQuery.getParams().add(offset);
            List<Product> listAllProducts = CRUDOperations.select(c, searchQuery.getSql().toString(), productsResultSetHandler, searchQuery.getParams().toArray());
            ProductPriceFilter productPriceFilter = (ProductPriceFilter) request.getSession().getAttribute("priceSortFilter");
            if(productPriceFilter != null) {
                return productPriceFilter.sortProductListByPrice(listAllProducts);
            }
            return listAllProducts;
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage());
        }
    }
    protected SearchQuery buildSearchQuery(String selectFields, SearchForm form) {
        List<Object> params = new ArrayList<>();
        StringBuilder sql = new StringBuilder("select ");
        sql.append(selectFields).append(" from product p, category c, producer pr where pr.id=p.id_producer and c.id=p.id_category and (p.name ilike ? or p.description ilike ?)");
        params.add("%" + form.getQuery() + "%");
        params.add("%" + form.getQuery() + "%");
        CRUDOperations.populateSqlAndParams(sql, params, form.getCategories(), "c.id = ?");
        CRUDOperations.populateSqlAndParams(sql, params, form.getProducers(), "pr.id = ?");
        return new SearchQuery(sql, params);
    }
}
