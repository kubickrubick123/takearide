package service;
import org.apache.commons.dbcp2.BasicDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.abstractservice.AbstractService;
import service.abstractservice.OrderService;
import service.abstractservice.ProductService;
import javax.servlet.ServletContext;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class ServiceManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceManager.class);
    private Map<String, AbstractService> serviceMap = new LinkedHashMap<>();
    private final Properties applicationProperties = new Properties();
    private final BasicDataSource dataSource;

    public static ServiceManager getInstance(ServletContext context) {
        ServiceManager instance = (ServiceManager) context.getAttribute("SERVICE_MANAGER");
        if (instance == null) {
            instance = new ServiceManager(context);
            context.setAttribute("SERVICE_MANAGER", instance);
        }
        return instance;
    }

    public ServiceManager(ServletContext context) {
        loadApplicationProperties();
        dataSource = createDataSource();
        serviceMap.put("CaptchaService", new CaptchaService());
        serviceMap.put("UserService", new UserService(dataSource));
        serviceMap.put("ProductService", new ProductServiceImpl(dataSource));
        serviceMap.put("OrderService", new OrderServiceImpl(dataSource));
    }

    private BasicDataSource createDataSource() {
        BasicDataSource dataSource = new BasicDataSource();
        dataSource.setDefaultAutoCommit(false);
        dataSource.setRollbackOnReturn(true);
        dataSource.setDriverClassName(getApplicationProperty("db.driver"));
        dataSource.setUrl(getApplicationProperty("db.url"));
        dataSource.setUsername(getApplicationProperty("db.username"));
        dataSource.setPassword(getApplicationProperty("db.password"));
        dataSource.setInitialSize(Integer.parseInt(getApplicationProperty("db.pool.initSize")));
        dataSource.setMaxTotal(Integer.parseInt(getApplicationProperty("db.pool.maxSize")));
        return dataSource;
    }

    public void close() {
        try {
            dataSource.close();
        } catch (SQLException e) {
            LOGGER.error("Close datasourse failed" + e.getMessage(), e);
        }
    }

    private void loadApplicationProperties() {
        try (InputStream in = ServiceManager.class.getClassLoader().getResourceAsStream("application.properties")) {
            applicationProperties.load(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String getApplicationProperty(String key) {
        return applicationProperties.getProperty(key);
    }

    public CaptchaService getCaptchaService() {
        return (CaptchaService) serviceMap.get("CaptchaService");
    }

    public UserService getUserService() {
        return (UserService) serviceMap.get("UserService");
    }

    public ProductService getProductService() {
        return (ProductService) serviceMap.get("ProductService");
    }

    public OrderService getOrderService() {
        return (OrderService) serviceMap.get("OrderService");
    }

}
