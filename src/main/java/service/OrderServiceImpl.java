package service;
import entity.Order;
import entity.OrderItem;
import entity.Product;
import entity.User;
import exception.AccessDeniedException;
import exception.InternalServerErrorException;
import form.ProductForm;
import form.ShoppingCartItem;
import jdbc.CRUDOperations;
import jdbc.ResultSetHandler;
import jdbc.ResultSetHandlerFactory;
import model.ShoppingCart;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.abstractservice.OrderService;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class OrderServiceImpl implements OrderService {
    private static final Logger LOGGER = LoggerFactory.getLogger(OrderServiceImpl.class);
    private final DataSource dataSource;
    private static final ResultSetHandler<Product> productResultSetHandler = ResultSetHandlerFactory.
            getSingleResultSetHandler(ResultSetHandlerFactory.PRODUCT_RESULT_SET_HANDLER);
    private final ResultSetHandler<Order> orderResultSetHandler =
            ResultSetHandlerFactory.getSingleResultSetHandler(ResultSetHandlerFactory.ORDER_RESULT_SET_HANDLER);
    private final ResultSetHandler<List<Order>> ordersResultSetHandler = ResultSetHandlerFactory.
            getListResultSetHandler(ResultSetHandlerFactory.ORDER_RESULT_SET_HANDLER);
    private final ResultSetHandler<List<OrderItem>> orderItemListResultSetHandler =
            ResultSetHandlerFactory.getListResultSetHandler(ResultSetHandlerFactory.ORDER_ITEM_RESULT_SET_HANDLER);
    private final String DEFAULT_ORDER_STATUS = "Accepted";
    public OrderServiceImpl(DataSource dataSource) {
        super();
        this.dataSource = dataSource;
    }

    @Override
    public void addProductToShoppingCart(ProductForm productForm, ShoppingCart shoppingCart) {
        try(Connection c = dataSource.getConnection()){
            Product product =CRUDOperations.select(c, "select p.*, c.name as category, pr.name as producer from product p, producer pr, category c "
                    + "where c.id=p.id_category and pr.id=p.id_producer and p.id=?", productResultSetHandler, productForm.getIdProduct());
            if(product == null) {
                throw new InternalServerErrorException("Product not found by id="+productForm.getIdProduct());
            }
            shoppingCart.addProduct(product, productForm.getCount());
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute sql query: " + e.getMessage());
        }
    }

    @Override
    public void removeProductFromShoppingCart(ProductForm productForm, ShoppingCart shoppingCart) {
       shoppingCart.removeProduct(productForm.getIdProduct(),productForm.getCount());
    }

    @Override
    public String serializeShoppingCart(ShoppingCart shoppingCart) {
        StringBuilder res = new StringBuilder();
        for (ShoppingCartItem item : shoppingCart.getItems()) {
            res.append(item.getProduct().getId()).append("-").append(item.getCount()).append("|");
        }
        if (res.length() > 0) {
            res.deleteCharAt(res.length() - 1);
        }
        return res.toString();
    }

    @Override
    public ShoppingCart deserializeShoppingCart(String string) {
        ShoppingCart shoppingCart = new ShoppingCart();
        String[] items = string.split("\\|");
        for (String item : items) {
            try {
                String data[] = item.split("-");
                int idProduct = Integer.parseInt(data[0]);
                int count = Integer.parseInt(data[1]);
                addProductToShoppingCart(new ProductForm(idProduct, count), shoppingCart);
            } catch (RuntimeException e) {
                LOGGER.error("Can't add product to ShoppingCart during deserialization: item=" + item, e);
            }
        }
        return shoppingCart.getItems().isEmpty() ? null : shoppingCart;
    }

    @Override
    public long makeOrder(ShoppingCart shoppingCart, User user,boolean isUserWantToPayByCash)  {
        if(shoppingCart == null || shoppingCart.getItems().isEmpty()) {
            throw  new InternalServerErrorException("shoppingCart is null or Empty");
        }
        try(Connection c = dataSource.getConnection()){
            Order order = CRUDOperations.insert(c,"insert into \"order\" values(nextval('order_seq'),?,?,?,?)",orderResultSetHandler,
                    user.getId(),new Timestamp(System.currentTimeMillis()),isUserWantToPayByCash,DEFAULT_ORDER_STATUS);
            CRUDOperations.insertBatch(c,"insert into order_item values(nextval('order_item_seq'),?,?,?)",
            toOrderParameterList(order.getId(),shoppingCart.getItems()));
            c.commit();
            return order.getId();
        } catch (SQLException e) {
        throw new InternalServerErrorException("Can't execute SQL request: " + e.getMessage(), e);
    }
    }


    private List<Object[]> toOrderParameterList(long idOrder, Collection<ShoppingCartItem> items) {
        List<Object[]> parametersList = new ArrayList<>();
        for(ShoppingCartItem item : items) {
            parametersList.add(new Object[]{idOrder,item.getProduct().getId(),item.getCount()});
        }
        return parametersList;
    }

    @Override
    public List<Order> listMyOrders(User user,int page,int limit) {
        try(Connection c = dataSource.getConnection()){
            int offset = (page - 1) * limit;
             return  CRUDOperations.select(c,"select * from \"order\"  where id_account=? order by id desc limit ? offset ?", ordersResultSetHandler, user.getId(), limit, offset);
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute SQL request: " + e.getMessage(), e);
        }
    }

    public Order findOrderById(long id,User user) {
        try (Connection c = dataSource.getConnection()) {
            Order order = CRUDOperations.select(c, "select * from \"order\" where id=?", orderResultSetHandler, id);
            if (!order.getIdAccount().equals(user.getId())) {
                throw new AccessDeniedException("Account with id=" + user.getId() + " is not owner for order with id=" + id);
            }
            List<OrderItem> list = CRUDOperations.select(c,
                    "select o.id as oid, o.id_order as id_order, o.id_product, o.count, p.*, c.name as category, pr.name as producer from order_item o, product p, category c, producer pr "
                            + "where pr.id=p.id_producer and c.id=p.id_category and o.id_product=p.id and o.id_order=?",
                    orderItemListResultSetHandler, id);
            order.setItems(list);
            return order;
        } catch (SQLException e) {
            throw new InternalServerErrorException("Can't execute SQL request: " + e.getMessage(), e);
        }
    }
}
