package filter;

import constants.Constants;
import entity.User;
import util.RoutingUtils;
import util.SessionUtils;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@WebFilter(filterName = "PermissionCheckerFilter")
public class PermissionCheckerFilter extends AbstractFilter {
    private Map<String, List<String>> permissionCheckerMap;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        permissionCheckerMap = (Map<String, List<String>>) filterConfig.getServletContext().getAttribute(Constants.PERMISSION_CHECKER_MAP);
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String requestURI = request.getRequestURI();
        List<String> checkersList = permissionCheckerMap.get(requestURI);
        if (checkersList != null) {
            User currentUser = SessionUtils.getCurrentAccount(request);
            if (currentUser != null) {
                if (checkersList.contains(currentUser.getUserRole())) {
                    chain.doFilter(request, response);
                } else {
                    request.setAttribute("statusCode", HttpServletResponse.SC_FORBIDDEN);
                    RoutingUtils.forwardToPage("error.jsp", request, response);
                }
            } else {
                request.getSession().setAttribute("target", request.getRequestURI());
                RoutingUtils.redirect("/signIn", request, response);
            }
        } else {
            chain.doFilter(request, response);
        }
    }
}
