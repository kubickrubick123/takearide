package filter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

@WebFilter(filterName = "ResponseHeaderFilter")
public class ResponseHeaderFilter extends AbstractFilter {

    private final Map<String, String> httpParams = new HashMap();

    public void init(FilterConfig filterConfig) throws ServletException {
        Enumeration enums = filterConfig.getInitParameterNames();
        while (enums.hasMoreElements()) {
            final String name = (String) enums.nextElement();
            final String value = filterConfig.getInitParameter(name);
            httpParams.put(name, value);
        }
    }

    @Override
    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        for (Map.Entry<String, String> entry : httpParams.entrySet()) {
            response.setHeader(entry.getKey(), entry.getValue());
        }
        chain.doFilter(request, response);
    }
}
