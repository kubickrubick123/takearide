package filter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.zip.GZIPOutputStream;

@WebFilter(filterName = "TrimResponseFilter")
public class TrimResponseFilter extends AbstractFilter {
    @Override
    public void doFilter(HttpServletRequest req,
                         HttpServletResponse resp, FilterChain chain) throws IOException, ServletException {
        if (isGzipAllowed(req)) {
            TrimResponse response = new TrimResponse(resp);
            response.setHeader("Content-Encoding", "gzip");
            chain.doFilter(req, response);
            response.close();
        } else {
            chain.doFilter(req, resp);
        }
    }

    private boolean isGzipAllowed(HttpServletRequest request) {
        return request.getHeader("Accept-Encoding").contains("gzip");
    }

    private static class TrimResponse extends HttpServletResponseWrapper {
        private GZipServletOutputStream gzipOutputStream = null;
        private PrintWriter printWriter = null;

        private TrimResponse(HttpServletResponse response) throws IOException {
            super(response);
        }

        public void close() throws IOException {
            if (this.printWriter != null) {
                this.printWriter.close();
            }
            if (this.gzipOutputStream != null) {
                this.gzipOutputStream.close();
            }
        }

        @Override
        public PrintWriter getWriter() throws IOException {
            if (this.printWriter == null && this.gzipOutputStream != null) {
                throw new IllegalStateException("OutputStream obtained already - cannot get PrintWriter");
            }
            if (this.printWriter == null) {
                this.gzipOutputStream = new GZipServletOutputStream(getResponse().getOutputStream());
                this.printWriter = new PrintWriter(new OutputStreamWriter(this.gzipOutputStream, getResponse().getCharacterEncoding()));
            }
            return this.printWriter;
        }

        @Override
        public ServletOutputStream getOutputStream() throws IOException {
            if (this.printWriter != null) {
                throw new IllegalStateException("PrintWriter obtained already - cannot get OutputStream");
            }
            if (this.gzipOutputStream == null) {
                this.gzipOutputStream = new GZipServletOutputStream(getResponse().getOutputStream());
            }
            return this.gzipOutputStream;
        }
    }

    private static class GZipServletOutputStream extends ServletOutputStream {
        private GZIPOutputStream gzipOutputStream = null;

        public GZipServletOutputStream(OutputStream output)
                throws IOException {
            super();
            this.gzipOutputStream = new GZIPOutputStream(output);
        }

        @Override
        public void close() throws IOException {
            this.gzipOutputStream.close();
        }

        @Override
        public void flush() throws IOException {
            this.gzipOutputStream.flush();
        }

        @Override
        public void write(byte b[]) throws IOException {
            this.gzipOutputStream.write(b);
        }

        @Override
        public void write(byte b[], int off, int len) throws IOException {
            this.gzipOutputStream.write(b, off, len);
        }

        @Override
        public void write(int b) throws IOException {
            this.gzipOutputStream.write(b);
        }

        @Override
        public boolean isReady() {
            return false;
        }

        @Override
        public void setWriteListener(WriteListener writeListener) {
        }
    }
}
