package filter;
import constants.Constants;
import requestwrapper.LocaleHeaderRequestWrapper;
import strategy.captchastrategy.languagestoragestrategy.AbstractLanguageStrategy;
import strategy.captchastrategy.languagestoragestrategy.CookieStorageStrategy;
import strategy.captchastrategy.languagestoragestrategy.SessionStorageStrategy;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Map;
import java.util.function.Supplier;

@WebFilter(filterName = "LocaleFilter")
public class LocaleFilter implements Filter {
    public Collection<Locale> AVAILABLE_LOCALES;
    protected Map<String, Supplier<AbstractLanguageStrategy>> langSupplierMap = new LinkedHashMap<>();
    private AbstractLanguageStrategy strategy;

    @Override
    public void init(FilterConfig filterConfig) {
        initStrategyMap();
        AVAILABLE_LOCALES = stringToLocale(filterConfig.getInitParameter("locales").split(","));
        strategy = langSupplierMap.get(filterConfig.getInitParameter("languageStorageStrategy")).get();
    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        String newLang = req.getParameter("lang");

        LocaleHeaderRequestWrapper mutableRequest = new LocaleHeaderRequestWrapper(req, (HttpServletResponse) servletResponse,
                findAcceptableLocale(Collections.list(req.getLocales())), newLang, strategy);
        filterChain.doFilter(mutableRequest, servletResponse);
    }

    @Override
    public void destroy() {
    }

    private Collection<Locale> stringToLocale(String[] strings) {
        LinkedList<Locale> locales = new LinkedList<>();
        for (String s : strings) {
            Locale l = new Locale(s);
            if (Constants.WEB_APP_AVAILABLE_LOCALES.contains(l)) {
                locales.add(l);
            }
        }
        return locales.isEmpty() ? Constants.WEB_APP_AVAILABLE_LOCALES : locales;
    }

    private Locale findAcceptableLocale(Collection<Locale> userLocales) {
        for (Locale locale : userLocales) {
            if (AVAILABLE_LOCALES.contains(locale)) {
                return locale;
            }
        }
        return Constants.DEFAULT_LOCALE;
    }

    public void initStrategyMap() {
        langSupplierMap.put("cookie", () -> new CookieStorageStrategy());
        langSupplierMap.put("session", () -> new SessionStorageStrategy());
    }
}
