package producFilter;
import entity.Product;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;

public class ProductPriceFilter {
    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    public ProductPriceFilter(int minPrice, int maxPrice) {
        this.minPrice = BigDecimal.valueOf(minPrice);
        this.maxPrice = BigDecimal.valueOf(maxPrice);
    }

    public List<Product> sortProductListByPrice(List<Product> productList) {
        List<Product> result = new LinkedList<>();
        for(Product p : productList) {
            if(p.getPrice().compareTo(maxPrice) == -1 && p.getPrice().compareTo(minPrice) == 1) {
                result.add(p);
            }
        }
        return result;
    }
}
