package listener;
import constants.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ServiceManager;
import util.XMLpermissionFileParser;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListener implements ServletContextListener {
    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationListener.class);
    private ServiceManager serviceManager;
    public void contextInitialized(ServletContextEvent sce) {
        try {
            serviceManager = ServiceManager.getInstance(sce.getServletContext());
            sce.getServletContext().setAttribute(Constants.CATEGORY_LIST, serviceManager.getProductService().listAllCategories());
            sce.getServletContext().setAttribute(Constants.PRODUCER_LIST, serviceManager.getProductService().listAllProducers());
            sce.getServletContext().setAttribute(Constants.CAPTCHA_SERVICE, serviceManager.getCaptchaService());
            String XMLfileName = sce.getServletContext().getInitParameter("PermissionFileName");
            sce.getServletContext().setAttribute(Constants.PERMISSION_CHECKER_MAP, new XMLpermissionFileParser().buildCheckersMapFromXml(XMLfileName));
        }catch (RuntimeException e){
            LOGGER.error("Web application 'ishop' init failed:"+e.getMessage(), e);
            throw e;
        }
        LOGGER.info("Web application 'ishop' initialized");
    }
    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOGGER.info("Web application 'ishop' destroyed");
    }
}
