package entity;

import java.util.Objects;

public class User extends AbstractEntity<Integer> {
    private String firstName;

    private String login;

    private String lastName;

    private String password;

    private String email;

    private boolean emailNotificationsAccepted;

    private String imageLink;

    private String userFailedCauseMessage;

    private String userRole;

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserFailedCauseMessage() {
        return userFailedCauseMessage;
    }

    public void setUserFailedCauseMessage(String userFailedCauseMessage) {
        this.userFailedCauseMessage = userFailedCauseMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return emailNotificationsAccepted == user.emailNotificationsAccepted &&
                Objects.equals(firstName, user.firstName) &&
                Objects.equals(login, user.login) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, login, lastName, password, email, emailNotificationsAccepted);
    }


    public String getImageLink() {
        return imageLink;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setEmailNotificationsAccepted(boolean emailNotificationsAccepted) {
        this.emailNotificationsAccepted = emailNotificationsAccepted;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public User() {

    }

    public User(String firstName, String login, String lastName, String password, String email, boolean emailNotificationsAccepted, String imageLink) {
        this.firstName = firstName;
        this.login = login;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
        this.emailNotificationsAccepted = emailNotificationsAccepted;
        this.imageLink = imageLink;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLogin() {
        return login;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public boolean isEmailNotificationsAccepted() {
        return emailNotificationsAccepted;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", emailNotificationsAccepted=" + emailNotificationsAccepted +
                '}';
    }

}
