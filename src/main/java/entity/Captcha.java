package entity;

public class Captcha {
    private String captchaCode;

    public Captcha(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    public String getCaptchaCode() {
        return captchaCode;
    }
}
