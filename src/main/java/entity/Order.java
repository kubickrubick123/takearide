package entity;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
public class Order extends AbstractEntity<Long> {
    private static final long serialVersionUID = 3026083684140455633L;
    private Integer idAccount;
    private List<OrderItem> items;
    private Timestamp created;
    private boolean isUserWantToPayByCash;
    private String orderStatus;
    private String orderStatusMessage;

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderStatusMessage() {
        return orderStatusMessage;
    }

    public void setOrderStatusMessage(String orderStatusMessage) {
        this.orderStatusMessage = orderStatusMessage;
    }

    public Order() {
    }

    public Integer getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(Integer idAccount) {
        this.idAccount = idAccount;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public boolean isUserWantToPayByCash() {
        return isUserWantToPayByCash;
    }

    public void setUserWantToPayByCash(boolean userWantToPayByCash) {
        isUserWantToPayByCash = userWantToPayByCash;
    }

    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    public BigDecimal getTotalCost() {
        BigDecimal cost = BigDecimal.ZERO;
        if (items != null) {
            for (OrderItem item : items) {
                cost = cost.add(item.getProduct().getPrice().multiply(BigDecimal.valueOf(item.getCount())));
            }
        }
        return cost;
    }

    @Override
    public String toString() {
        return String.format("Order [id=%s, idAccount=%s, items=%s, created=%s]", getId(), idAccount, items, created);
    }
}
