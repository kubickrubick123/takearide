package servlet.page;
import entity.Product;
import servlet.controller.AbstractController;
import util.RoutingUtils;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/products")
public class AllProductsController extends AbstractController {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Product> products = getProductService().listAllProducts(getPage(req), productsPerPage(req),req);
        req.setAttribute("page",getPage(req));
        req.setAttribute("products", products);
        int totalCount = getProductService().countAllProducts();
        req.setAttribute("pageCount", getPageCount(totalCount, productsPerPage(req)));
        RoutingUtils.forwardToPage("products.jsp", req, resp);
    }
}
