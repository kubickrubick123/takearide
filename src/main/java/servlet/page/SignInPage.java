package servlet.page;
import constants.Constants;
import servlet.controller.AbstractController;
import util.RoutingUtils;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/signIn")
public class SignInPage extends AbstractController {
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getSession().getAttribute(Constants.CURRENT_ACCOUNT) != null) {
            String targetUrl = (String) req.getSession().getAttribute("target");
            req.getSession().removeAttribute("target");
            if(targetUrl != null) {
                RoutingUtils.redirect(targetUrl,req,resp);
            }else {
                RoutingUtils.redirect("/products",req,resp);
            }
        } else {
            RoutingUtils.forwardToPage("signInPage.jsp", req, resp);
        }
    }
}
