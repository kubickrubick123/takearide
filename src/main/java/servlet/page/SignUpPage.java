package servlet.page;
import strategy.captchastrategy.AbstractStrategy;
import constants.Constants;
import entity.Captcha;
import servlet.controller.AbstractController;
import util.CaptchaValueGenerator;
import util.RoutingUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/signup")
public class SignUpPage extends AbstractController {
    private AbstractStrategy strategy;
    private final String HIDDEN_FIELD = "hiddenField";

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        handleRequest(request, response);
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        handleRequest(req, resp);
    }

    private void handleRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        isHiddenFiledNeeded(req);
        strategy = getCaptchaStrategy();
        strategy.deleteCaptchaValue(req);
        Captcha captcha = new Captcha(CaptchaValueGenerator.generateValue());
        req.getSession().setAttribute("Captcha", captcha);
        strategy.saveCaptchaValue(captcha.getCaptchaCode(), req, resp);
        req.getSession().setAttribute(Constants.CURRENT_TIME, System.currentTimeMillis());
        RoutingUtils.forwardToPage("signupPage.jsp", req, resp);
    }

    private void isHiddenFiledNeeded(HttpServletRequest request) {
        if (HIDDEN_FIELD.equals(getServletContext().getInitParameter("strategy"))) {
            request.getSession().setAttribute("hiddenField", HIDDEN_FIELD);
        }
    }
}
