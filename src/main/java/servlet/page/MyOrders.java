package servlet.page;
import constants.Constants;
import entity.User;
import servlet.controller.AbstractController;
import util.RoutingUtils;
import util.SessionUtils;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/my-orders")
public class MyOrders extends AbstractController {
    private static final long serialVersionUID = -1782066337808445826L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = SessionUtils.getCurrentAccount(req);
        req.setAttribute("orders",getOrderService().listMyOrders(user,getPage(req), Constants.MAX_PRODUCTS_PER_HTML_PAGE));
        RoutingUtils.forwardToPage("my-orders.jsp", req, resp);
    }
}

