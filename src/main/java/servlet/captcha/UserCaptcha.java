package servlet.captcha;
import entity.Captcha;
import servlet.controller.AbstractController;
import util.CaptchaDrawer;
import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

@WebServlet("/captcha")
public class UserCaptcha extends AbstractController {
    private static final long serialVersionUID=1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)throws IOException {
      processRequest(request,response);
    }

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("image/jpg");
        Captcha  captcha = (Captcha) request.getSession().getAttribute("Captcha");
        request.getSession().removeAttribute("Captcha");
        OutputStream osImage = response.getOutputStream();
        ImageIO.write(CaptchaDrawer.drawCaptcha(captcha), "jpeg", osImage);
        osImage.flush();
    }
}
