package servlet.ajax;
import form.ProductForm;
import model.ShoppingCart;
import servlet.controller.AbstractProductController;
import util.SessionUtils;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/ajax/json/product/remove")
public class RemoveProductController extends AbstractProductController {
    @Override
    protected void  processProductForm(ProductForm productForm, ShoppingCart shoppingCart, HttpServletRequest request, HttpServletResponse response) {
        getOrderService().removeProductFromShoppingCart(productForm,shoppingCart);
        if(shoppingCart.getItems().isEmpty()){
            SessionUtils.clearCurrentShoppingCart(request,response);
        }else {
            String cookieValue = getOrderService().serializeShoppingCart(shoppingCart);
            SessionUtils.updateCurrentShoppingCartCookie(cookieValue,response);
        }
    }
}
