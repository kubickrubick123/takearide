package servlet.ajax;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/resetPriceSort")
public class ResetPriceSortController  extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
       req.getSession().removeAttribute("priceSortFilter");
       req.getSession().removeAttribute("minprice");
       req.getSession().removeAttribute("maxprice");
    }
}
