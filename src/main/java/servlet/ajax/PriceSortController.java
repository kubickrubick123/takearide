package servlet.ajax;
import producFilter.ProductPriceFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/priceSort")
public class PriceSortController extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        String firstParameter = req.getParameter("minprice");
        String secondParameter = req.getParameter("maxprice");
        if(isRequestParametersAreNotNull(firstParameter,secondParameter)) {
            int minPrice = Integer.valueOf(firstParameter);
            int maxPrice = Integer.valueOf(secondParameter);
            req.getSession().setAttribute("minprice", minPrice);
            req.getSession().setAttribute("maxprice", maxPrice);
            req.getSession().setAttribute("priceSortFilter", new ProductPriceFilter(minPrice, maxPrice));
        }
    }

    private boolean isRequestParametersAreNotNull(String firstParameter,String secondParameter) {
        return firstParameter != "" && secondParameter != "" ? true : false;
    }
}
