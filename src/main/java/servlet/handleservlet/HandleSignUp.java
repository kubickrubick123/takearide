package servlet.handleservlet;
import strategy.captchastrategy.AbstractStrategy;
import entity.User;
import service.UserService;
import servlet.controller.AbstractController;
import util.RoutingUtils;
import util.SessionUtils;
import util.WebUtil;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/handleSignUp")
@MultipartConfig
public class HandleSignUp extends AbstractController {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserService userService = getUserService();
        AbstractStrategy strategy = getCaptchaStrategy();
        String requiredCaptchaValue = AbstractStrategy.keyCodeMap.get(strategy.getCaptchaValue(req));
        String inputCaptchaValue = req.getParameter("inChars");
        User user = userService.buildUserFromRequest(req);
        if (userService.isUserFieldsNotNull(user) && userService.checkUserLogin(user) && WebUtil.validateCaptcha(requiredCaptchaValue, inputCaptchaValue,user) && WebUtil.checkTimeOut(req,TIME_OUT,user)) {
            userService.saveUserToDataBase(user);
            SessionUtils.setCurrentAccount(req,user);
            RoutingUtils.redirect("/ishop/products",req,resp);
        } else {
            req.setAttribute("failedUser",user);
            req.getRequestDispatcher("/signup").forward(req, resp);
        }
    }
}
