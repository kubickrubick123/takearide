package servlet.handleservlet;

import constants.Constants;
import entity.User;
import servlet.controller.AbstractController;
import util.RoutingUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/handleSignIn")
public class HandleSignIn extends AbstractController {
    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = getUserService().getUser(req.getParameter("login"), req.getParameter("password1"));
        if (user != null) {
            req.getSession().setAttribute(Constants.CURRENT_ACCOUNT, user);
            RoutingUtils.redirect("/signIn", req, resp);
        } else {
            RoutingUtils.forwardToPage("signInPage.jsp", req, resp);
        }
    }
}
