package servlet.controller;
import constants.Constants;
import util.RoutingUtils;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/productPerPageController")
public class ProductPerPageController extends AbstractController {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().setAttribute(Constants.PRODUCTS_PER_PAGE,Integer.parseInt(req.getParameter("productCount")));
        RoutingUtils.redirect("/products", req, resp);
    }
}
