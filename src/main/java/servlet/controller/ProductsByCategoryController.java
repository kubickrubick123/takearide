package servlet.controller;
import entity.Product;
import util.RoutingUtils;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
@WebServlet("/products/*")
public class ProductsByCategoryController extends AbstractController {
    private static final int SUBSTRING_INDEX = "/products".length();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String categoryUrl = req.getRequestURI().substring(SUBSTRING_INDEX);
        List<Product> products = getProductService().listProductsByCategory(categoryUrl, getPage(req), productsPerPage(req),req);
        req.setAttribute("products", products);
        int totalCount = getProductService().countProductsByCategory(categoryUrl);
        req.setAttribute("pageCount", getPageCount(totalCount,productsPerPage(req)));
        req.setAttribute("selectedCategoryUrl", categoryUrl);
        RoutingUtils.forwardToPage("products.jsp", req, resp);
    }
}