package servlet.controller;
import form.ProductForm;
import model.ShoppingCart;
import org.json.JSONObject;
import util.RoutingUtils;
import util.SessionUtils;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public abstract class AbstractProductController extends AbstractController {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ProductForm form = createProductForm(req);
        ShoppingCart shoppingCart = SessionUtils.getCurrentShoppingCart(req);
        processProductForm(form, shoppingCart, req, resp);
        sendResponse(shoppingCart, req, resp);

    }

    protected abstract void processProductForm(ProductForm productForm,ShoppingCart shoppingCart,HttpServletRequest request,HttpServletResponse response);

    protected void  sendResponse(ShoppingCart shoppingCart,HttpServletRequest request,HttpServletResponse response) throws IOException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("totalCount",shoppingCart.getTotalCount());
        jsonObject.put("totalCost",shoppingCart.getTotalCost());
        RoutingUtils.sendJSON(jsonObject,request,response);
    }
}
