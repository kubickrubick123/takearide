package servlet.controller;

import strategy.captchastrategy.AbstractStrategy;
import constants.Constants;
import form.ProductForm;
import form.SearchForm;
import service.CaptchaService;
import service.ServiceManager;
import service.UserService;
import service.abstractservice.OrderService;
import service.abstractservice.ProductService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;

public abstract class AbstractController extends HttpServlet {
    private CaptchaService captchaService;
    private AbstractStrategy captchaStrategy;
    private UserService userService;
    private OrderService orderService;
    private ProductService productService;
    protected long TIME_OUT;

    public AbstractStrategy getCaptchaStrategy() {
        return captchaStrategy;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        orderService = (OrderService) ServiceManager.getInstance(getServletContext()).getOrderService();
        productService = ServiceManager.getInstance(getServletContext()).getProductService();
        userService = (UserService) ServiceManager.getInstance(getServletContext()).getUserService();
        captchaService = (CaptchaService) ServiceManager.getInstance(getServletContext()).getCaptchaService();
        captchaStrategy = getCaptchaService().getCaptchaStrategyMap().get(getServletContext().getInitParameter("strategy")).get();
        TIME_OUT = Long.parseLong(getServletContext().getInitParameter("signUp-Timeout"));
    }

    public CaptchaService getCaptchaService() {
        return captchaService;
    }

    public UserService getUserService() {
        return userService;
    }

    public OrderService getOrderService() {
        return orderService;
    }

    public ProductService getProductService() {
        return productService;
    }

    public final int getPage(HttpServletRequest request) {
        if (request.getParameter("page") == null) {
            return 1;
        } else {
            return Integer.parseInt(request.getParameter("page"));
        }
    }

    public final int getPageCount(int totalCount, int itemsPerPage) {
        int res = totalCount / itemsPerPage;
        if (res * itemsPerPage != totalCount) {
            res++;
        }
        return res;
    }

    public final SearchForm createSearchForm(HttpServletRequest request) {
        return new SearchForm(
                request.getParameter("query"),
                request.getParameterValues("category"),
                request.getParameterValues("producer"));
    }

    public final ProductForm createProductForm(HttpServletRequest request) {
        return new ProductForm(Integer.parseInt(request.getParameter("idProduct")),
                Integer.parseInt(request.getParameter("count")));
    }

    protected int productsPerPage(HttpServletRequest request) {
        Integer number = (Integer) request.getSession().getAttribute(Constants.PRODUCTS_PER_PAGE);
        return number != null ? number : Constants.DEFAULT_VALUE_PRODUCTS_PER_PAGE;
    }
}

