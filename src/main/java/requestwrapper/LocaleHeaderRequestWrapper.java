package requestwrapper;
import strategy.captchastrategy.languagestoragestrategy.AbstractLanguageStrategy;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public final class LocaleHeaderRequestWrapper extends HttpServletRequestWrapper {
    private HttpServletRequest httpServletRequest;
    private HttpServletResponse httpServletResponse;
    private Locale locale;
    private String newLang;
    private AbstractLanguageStrategy languageStrategy;
    public  LocaleHeaderRequestWrapper(HttpServletRequest request, HttpServletResponse response, Locale locale, String newLang,
                                       AbstractLanguageStrategy languageStrategy){
        super(request);
        this.newLang = newLang;
        this.languageStrategy = languageStrategy;
        this.locale = locale;
        httpServletRequest = request;
        httpServletResponse = response;
    }

    @Override
    public Locale getLocale() {
       return languageStrategy.rememberAndGetLocale(httpServletRequest,httpServletResponse,locale,newLang);
    }
}
