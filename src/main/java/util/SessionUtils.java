package util;
import constants.Constants;
import entity.User;
import model.ShoppingCart;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SessionUtils {
    public static ShoppingCart getCurrentShoppingCart(HttpServletRequest req) {
        ShoppingCart shoppingCart = (ShoppingCart) req.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART);
        if (shoppingCart == null) {
            shoppingCart = new ShoppingCart();
            setCurrentShoppingCart(req, shoppingCart);
        }
        return shoppingCart;
    }
    public static boolean isCurrentShoppingCartCreated(HttpServletRequest req) {
        return req.getSession().getAttribute(Constants.CURRENT_SHOPPING_CART) != null;
    }
    public static void updateCurrentShoppingCartCookie(String cookieValue, HttpServletResponse resp) {
        WebUtil.setCookie(Constants.Cookie.SHOPPING_CART.getName(), cookieValue,
                Constants.Cookie.SHOPPING_CART.getTtl(), resp);
    }
    public static void setCurrentShoppingCart(HttpServletRequest req, ShoppingCart shoppingCart) {
        req.getSession().setAttribute(Constants.CURRENT_SHOPPING_CART, shoppingCart);
    }
    public static void clearCurrentShoppingCart(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().removeAttribute(Constants.CURRENT_SHOPPING_CART);
        WebUtil.setCookie(Constants.Cookie.SHOPPING_CART.getName(), null, 0, resp);
    }
    public static User getCurrentAccount(HttpServletRequest req) {
        return (User) req.getSession().getAttribute(Constants.CURRENT_ACCOUNT);
    }

    public static void setCurrentAccount(HttpServletRequest req, User currentAccount) {
        req.getSession().setAttribute(Constants.CURRENT_ACCOUNT, currentAccount);
    }
    public static Cookie findShoppingCartCookie(HttpServletRequest req) {
        return WebUtil.findCookie(req, Constants.Cookie.SHOPPING_CART.getName());
    }

    public static boolean isCurrentAccountCreated(HttpServletRequest request) {
        return getCurrentAccount(request) != null;
    }
}
