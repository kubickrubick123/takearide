package util;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class XMLpermissionFileParser {
    public  Map<String, List<String>> buildCheckersMapFromXml(String fileName) {
        NodeList nodeList = getNodesFromXML(new File(fileName));
        Map<String,List<String>> checkerMap = new HashMap<>();
        for (int nodeElement = 0; nodeElement < nodeList.getLength();nodeElement++) {
            Element eElement = (Element) nodeList.item(nodeElement);
            String url = eElement.getElementsByTagName("url-pattern").item(0).getTextContent();
            checkerMap.put(url,getArrayFromElements(eElement.getElementsByTagName("role")));
        }
        return checkerMap;
    }

    private static List getArrayFromElements(NodeList nodeList) {
        List<String> list = new LinkedList<>();
        for(int index = 0; index < nodeList.getLength(); index ++) {
            list.add(nodeList.item(index).getTextContent());
        }
        return list;
    }

    private static NodeList getNodesFromXML(File xmlFile) {
        Document document = null;
        try { ;
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            document = dBuilder.parse(xmlFile);
        } catch (IOException | ParserConfigurationException | SAXException e) {
            System.err.println(e + e.getMessage());
        }
        return document.getElementsByTagName("constraint");
    }
}
