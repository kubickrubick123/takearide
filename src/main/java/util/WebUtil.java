package util;
import constants.Constants;
import entity.User;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class WebUtil {
    public static Cookie findCookie(HttpServletRequest req, String cookieName) {
        Cookie[] cookies = req.getCookies();
        if (cookies != null) {
            for (Cookie c : cookies) {
                if (c.getName().equals(cookieName) && c.getValue() != null && !"".equals(c.getValue()) ) {
                        return c;
                }
            }
        }
        return null;
    }

    public static Cookie setCookie(String name, String value, int age, HttpServletResponse resp) {
        Cookie c = new Cookie(name, value);
        c.setMaxAge(age);
        c.setPath("/");
        c.setHttpOnly(true);
        resp.addCookie(c);
        return  c;
    }

    public static void updateCookieLifeTime(Cookie c,HttpServletRequest request,HttpServletResponse response) {
        deleteCookie(request,c.getName());
        setCookie(c.getName(),c.getValue(),c.getMaxAge(),response);
    }

    public static void deleteCookie(HttpServletRequest request, String cookieName) {
        Cookie foundedCookie = findCookie(request, cookieName);
        if (foundedCookie != null) {
            foundedCookie.setMaxAge(0);
        }
    }

    public static boolean validateCaptcha(String requiredValue, String inputValue, User user) {
         if(requiredValue.equals(inputValue)){
             return true;
         }
         user.setUserFailedCauseMessage(Constants.INCORRECT_CAPTCHA_VALUE);
         return false;
    }

    public static boolean checkTimeOut(HttpServletRequest request, long timeOut,User user) {
        long generateSingUpPageTime = (long) request.getSession().getAttribute(Constants.CURRENT_TIME);
        if (System.currentTimeMillis() - generateSingUpPageTime > timeOut) {
            user.setUserFailedCauseMessage(Constants.TIME_OUT);
            return false;
        }
        return true;
    }
    public static String getCurrentRequestUrl(HttpServletRequest req) {
        String query = req.getQueryString();
        if (query == null) {
            return req.getRequestURI();
        } else {
            return req.getRequestURI() + "?" + query;
        }
    }

}