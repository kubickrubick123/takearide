package util;

import java.util.Random;

public class CaptchaValueGenerator {
    private static Random randChars = new Random();

    public static String generateValue() {
        int iTotalChars = 6;
        return (Long.toString(Math.abs(randChars.nextLong()), 36)).substring(0, iTotalChars);
    }
}
