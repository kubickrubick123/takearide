package util;
import entity.Captcha;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

public class CaptchaDrawer {
    private static Random randChars = new Random();
    public static BufferedImage drawCaptcha(Captcha captcha) {
        int iTotalChars = 6;
        int iHeight = 40;
        int iWidth = 150;
        Font fntStyle1 = new Font("Arial", Font.BOLD, 30);
        String sImageCode = captcha.getCaptchaCode();
        BufferedImage biImage = new BufferedImage(iWidth, iHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2dImage = (Graphics2D) biImage.getGraphics();
        int iCircle = 15;
        for (int i = 0; i < iCircle; i++) {
            g2dImage.setColor(new Color(randChars.nextInt(255), randChars.nextInt(255), randChars.nextInt(255)));
        }
        g2dImage.setFont(fntStyle1);
        for (int i = 0; i < iTotalChars; i++) {
            g2dImage.setColor(new Color(randChars.nextInt(255), randChars.nextInt(255), randChars.nextInt(255)));
            g2dImage.drawString(sImageCode.substring(i, i + 1), 25 * i, 24);
        }
        g2dImage.dispose();
        return biImage;
    }
}
