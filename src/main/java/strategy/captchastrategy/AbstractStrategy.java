package strategy.captchastrategy;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;

public abstract class AbstractStrategy {
    protected Random randomKey = new Random();

     public static Map<String,String> keyCodeMap = new LinkedHashMap<>();

     public  abstract void saveCaptchaValue(String captchaValue,HttpServletRequest request,HttpServletResponse response);

     public abstract String getCaptchaValue(HttpServletRequest request);

     public abstract void deleteCaptchaValue(HttpServletRequest request);
}
