package strategy.captchastrategy;
import constants.Constants;

import javax.servlet.http.HttpServletRequest;

public class HiddenFieldCaptchaStrategy extends AttributeCaptchaStrategy {

    @Override
    public String getCaptchaValue(HttpServletRequest request) {
        return request.getParameter(Constants.CAPTCHA_CODE);
    }
}
