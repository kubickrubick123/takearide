package strategy.captchastrategy.languagestoragestrategy;
import constants.Constants;
import util.WebUtil;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class CookieStorageStrategy extends AbstractLanguageStrategy {
    @Override
    public Locale rememberAndGetLocale(HttpServletRequest request, HttpServletResponse response,Locale locale, String lang) {
        Cookie cookie = WebUtil.findCookie(request,"locale");
       if(cookie == null) {
            WebUtil.setCookie("locale",locale.toString(), Constants.COOKIE_DEFAULT_LIFE_TIME,response);
           return locale;
        }
       if(lang != null) {
          return  new Locale(WebUtil.setCookie("locale",lang,Constants.COOKIE_DEFAULT_LIFE_TIME,response).getValue());
       }
        WebUtil.updateCookieLifeTime(cookie,request,response);
        return new Locale(cookie.getValue());
    }
}