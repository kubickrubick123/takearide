package strategy.captchastrategy.languagestoragestrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public class SessionStorageStrategy extends AbstractLanguageStrategy{
    @Override
    public Locale rememberAndGetLocale(HttpServletRequest request, HttpServletResponse response, Locale locale, String lang) {
        Locale establishedLocale = (Locale) request.getSession().getAttribute("locale");
        if(establishedLocale == null) {
            request.getSession().setAttribute("locale",locale);
            return locale;
        }
        if(lang != null) {
            Locale newLocale = new Locale(lang);
            request.getSession().setAttribute("locale",newLocale);
            return newLocale;
        }
        return establishedLocale;
    }
}
