package strategy.captchastrategy.languagestoragestrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

public abstract class AbstractLanguageStrategy {
    public abstract Locale rememberAndGetLocale(HttpServletRequest request, HttpServletResponse response,Locale locale,String lang);
}
