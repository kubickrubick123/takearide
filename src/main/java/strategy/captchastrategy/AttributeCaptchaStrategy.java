package strategy.captchastrategy;
import constants.Constants;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AttributeCaptchaStrategy extends  AbstractStrategy {

    public  void saveCaptchaValue(String captchaValue,HttpServletRequest request,HttpServletResponse response) {
        String key = String.valueOf(randomKey.nextInt());
        keyCodeMap.put(key,captchaValue);
        request.getSession().setAttribute(Constants.CAPTCHA_CODE,key);
    }

    @Override
    public String getCaptchaValue(HttpServletRequest request) {
        return String.valueOf(request.getSession().getAttribute(Constants.CAPTCHA_CODE));
    }

    @Override
    public void deleteCaptchaValue(HttpServletRequest request) {
        keyCodeMap.clear();
        request.getSession().removeAttribute(Constants.CAPTCHA_CODE);
    }
}
