package strategy.captchastrategy;
import constants.Constants;
import util.WebUtil;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookieCaptchaStrategy extends AbstractStrategy{

    @Override
    public void saveCaptchaValue(String captchaValue,HttpServletRequest request,HttpServletResponse response) {
        String key = String.valueOf(randomKey.nextInt());
        keyCodeMap.put(key,captchaValue);
        WebUtil.setCookie(Constants.CAPTCHA_CODE,key,1800,response);
    }

    @Override
    public String getCaptchaValue(HttpServletRequest request) {
        return WebUtil.findCookie(request,Constants.CAPTCHA_CODE).getValue();
    }

    @Override
    public void deleteCaptchaValue(HttpServletRequest request) {
        keyCodeMap.clear();
        WebUtil.deleteCookie(request,Constants.CAPTCHA_CODE);
    }

}
