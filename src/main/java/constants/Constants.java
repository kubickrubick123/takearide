package constants;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

public class Constants {
    public static final String CURRENT_ACCOUNT = "CURRENT_ACCOUNT";
    public static final String CAPTCHA_SERVICE = "Captcha Service";
    public static final String CAPTCHA_CODE = "captcha_code";
    public static final String CURRENT_TIME = "current_time";
    public static final String LOGIN_ALREADY_IN_USE = "Login already Exists";
    public static final String INCORRECT_USER_DATA = "Incorrect login or password";
    public static final String PRODUCTS_PER_PAGE = "productsPerPage";
    public static final int DEFAULT_VALUE_PRODUCTS_PER_PAGE = 12;
    public static final String INCORRECT_CAPTCHA_VALUE = "Incorrect Captcha Value";
    public static final String TIME_OUT = "TimeOut";
    public static final String CATEGORY_LIST = "CATEGORY_LIST";
    public static final String PRODUCER_LIST = "PRODUCER_LIST";
    public static final int MAX_PRODUCTS_PER_HTML_PAGE = 12;
    public static final int MAX_PRODUCT_COUNT_PER_SHOPPING_CART = 10;
    public static final int MAX_PRODUCTS_PER_SHOPPING_CART = 20;
    public static final String CURRENT_SHOPPING_CART = "CURRENT_SHOPPING_CART";
    public static final String SUCCESS_REDIRECT_URL_AFTER_SIGNIN = "SUCCESS_REDIRECT_URL_AFTER_SIGIN";
    public static final List<Locale> WEB_APP_AVAILABLE_LOCALES = new LinkedList<>(Arrays.asList(new Locale("en_US"),new Locale("es")));
    public static final Locale DEFAULT_LOCALE = new Locale("en");
    public static final int COOKIE_DEFAULT_LIFE_TIME = 60 * 60 * 24 * 365; //it is equals to one year
    public static final String PERMISSION_CHECKER_MAP  = "PERMISSION_CHECKER_MAP";
    public static final String DEFAULT_USER_ROLE = "client";
    public enum Cookie {
        SHOPPING_CART("iSCC", COOKIE_DEFAULT_LIFE_TIME);

        private final String name;
        private final int ttl;

        private Cookie(String name, int ttl) {
            this.name = name;
            this.ttl = ttl;
        }

        public String getName() {
            return name;
        }

        public int getTtl() {
            return ttl;
        }
    }
}
