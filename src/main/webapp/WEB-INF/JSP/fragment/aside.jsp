<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="ishop" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${pageContext.request.locale}"  scope="session"/>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language" />
<div class="visible-xs-block xs-option-container">
	<a class="pull-right" data-toggle="collapse" href="#productCatalog">Product catalog <span class="caret"></span></a>
	<a data-toggle="collapse" href="#findProducts">Find products <span class="caret"></span></a>
</div>
<form class="search" action="/search">
	<div id="findProducts" class="panel panel-success collapse">
		<div class="panel-heading"><fmt:message key="label.findproducts" /></br></div>
		<div class="panel-body">
			<div class="input-group">
				<input type="text" name="query" class="form-control" placeholder="Search query" value="${searchForm.query }">
				<span class="input-group-btn">
					<a id="goSearch" class="btn btn-default"><fmt:message key="label.Go" /></br></a>
				</span>
			</div>
			<div class="more-options">
				<a data-toggle="collapse" href="#searchOptions"><fmt:message key="label.MoreFilters" /><span class="caret"></span></a>
			</div>
		</div>
		<div id="searchOptions" class="collapse">
			<ishop:category-filter categories="${CATEGORY_LIST }" />
			<ishop:producer-filter producers="${PRODUCER_LIST }" />
		</div>
	</div>
</form>
<div id="productCatalog" class="panel panel-success collapse">
	<div class="panel-heading"><fmt:message key="label.ProductCatalog" /></div>
	<div class="list-group">
		<c:forEach var="category" items="${CATEGORY_LIST }">
			<a href="/products${category.url }" class="list-group-item ${selectedCategoryUrl == category.url ? 'active' : '' }">
				<span class="badge">${category.productCount}</span> ${category.name}
			</a>
		</c:forEach>
		<form >
           <input type="number"  name="minprice" id="minprice" value="${sessionScope.minprice}"  required ><fmt:message key="label.MinPrice" /></input>
           <input type="number" name="maxprice" id="maxprice"  value="${sessionScope.maxprice}"  required><fmt:message key="label.MaxPrice" /></input>
           <input type="button"  id="submitSort" value =<fmt:message key="label.SubmitPriceRange" /> />
		</form>
		<input type="button" id="resetSort" value =<fmt:message key="label.ResetPriceRange"/> />
	</div>
	<form action="/productPerPageController" method="post"  id="productCountForm" style="margin-top:5px;float:left;">
                    <p >Products per Page</p>
                    <select  name ="productCount" id="productCount">
                     <option disabled>Products per page</option>
                      <option value="24">24</option>
                      <option value ="12">12</option>
                      <option value ="12">8</option>
                      <option value ="6">6</option>
                      <option value ="4">4</option>
                      <option value = "2">2</option>
                    </select>
                    <p><input type="submit" value = <fmt:message key="label.SaveValue" /> ></input</p>
                    </form>
</div>