<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag-files" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<head>
    <meta charset="UTF-8">
    <title>Ishop Sign-Up</title>
    <link href="static/css/registration-form.css" rel="stylesheet">
     <script src="static/js/jqueryValidator.js"></script>
</head>
<body>
<c:if test="${failedUser != null }">
		<div class="alert alert-success hidden-print" role="alert">${failedUser.userFailedCauseMessage}</div>
</c:if>
<div class="form-body">
    <form class="sign-up-form" action="/handleSignUp" method="post"  enctype="multipart/form-data">
        <p>Login</p>
             <input name="login" id="login" required value="${failedUser.login}">
        <p>First Name:</p>
            <input name="username" id="username" value="${failedUser.firstName}" required/><p id="validName"></p>
        <p>Last Name:</p>
            <input name="lastname" id="lastname" value="${failedUser.lastName}" required/>
        <p>Password:</p>
            <input type="password" name="password1" id="password1" required/><p id="validPassword"></p>
        <p>Repeat Password:
            <input type="password" name="password2" id="password2" required/><p id="validPassword"></p>
        <p>Email:</p>
            <input type="text" name="email" id="email" value="${failedUser.email}" required/><p id="validEmail"></p>
        <input type="checkbox" name="keeppost" >Keep me post on email<br>
        <input type="file" name="avatar" accept="image/*">
        <tag-files:captchatag/>
         <input type='text' name="inChars" value=''>
        <tag-files:hiddenfield/>
            <p><button  type="button" id="submit_button"  onclick="finalValidate(this.form)">Submit</button></p>
        </form>
  </div>
</body>
