<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="ishop" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<c:set var="language" value="${pageContext.request.locale}"  scope="session"/>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language" />
<html lang="${language}">
<div id="productList" data-page-count="${pageCount}" data-page-number="1">
	<div class="row">
		<jsp:include page="../fragment/product-list.jsp" />
	</div>
	<c:if test="${pageCount > 1 }">
	<div class="text-center hidden-print">
	<c:if test="${products.isEmpty() == false}">
        <a id="loadMore" class="btn btn-success"><fmt:message key="label.LoadMoreProducts"/></a>
	</div>
	</c:if>
	</c:if>
</div>
<ishop:add-product-popup />
