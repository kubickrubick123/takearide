<%@ page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="tag-files" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="language" value="${pageContext.request.locale}"  scope="session"/>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language" />
<html lang="${language}">
<head>
    <meta charset="UTF-8">
    <title>Ishop Sign-Up</title>
    <link href="static/css/registration-form.css" rel="stylesheet">
</head>
<body>
<c:if test="${message != null }">
		<div class="alert alert-success hidden-print" role="alert">${message}</div>
</c:if>
    <div class="alert alert-success hidden-print" role="alert">${locales}</div>
    <form class="sign-in-form" action="/handleSignIn" method="post">
       <fmt:message key="label.login" /></br>
             <input name="login" id="login" required value="${login}">
        <fmt:message key="label.password" />
            <input type="password" name="password1" id="password1" required/>
         <p> <input type="submit" id="submit_button_sign_in" /></p>
        </form>
</body>
</html>