<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${pageContext.request.locale}"  scope="session"/>
<fmt:setLocale value="${language}" />
<fmt:setBundle basename="language" />
<html lang="${language}">

<c:choose>
    <c:when test="${CURRENT_ACCOUNT == null}">
      <a href="/signup" class="btn btn-primary navbar-btn navbar-right sign-in"><i  aria-hidden="true"></i><fmt:message key="label.SignUp" /></a>
      <a href="/signIn" class="btn btn-primary navbar-btn navbar-right sign-in"><i  aria-hidden="true"></i><fmt:message key="label.SignIn" /></a>
    </c:when>
    <c:otherwise>
        <img src="/static/avatar/${CURRENT_ACCOUNT.imageLink}" class = "navbar-right" style = "border-radius: 3px;width: 40px;height: 40px;"/>
        <a href="/signOut" class="btn btn-primary navbar-btn navbar-right sign-in"><i  aria-hidden="true"></i><fmt:message key="label.SignOut" /></a>
    </c:otherwise>
</c:choose>