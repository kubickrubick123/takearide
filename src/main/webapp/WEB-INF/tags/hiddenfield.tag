<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:if test="${sessionScope.hiddenField != null}">
		<input type="hidden" name="captcha_code" value="${sessionScope.captcha_code}">
</c:if>
