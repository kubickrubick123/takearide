window.emailFlag = false;
window.nameFlag = false;
window.passwordFlag = false;

function finalValidate(form) {
    var nameFlag = false;
    var password1Flag = false;
    checkEmail();
    checkName();
    checkPasswords();
    checkFlags(form);
}

function checkFlags(form) {
    if (emailFlag == true && nameFlag == true && passwordFlag == true) {
        form.submit();
    }
}

function checkEmail() {
    var email_regexp = /[0-9a-zа-я_A-ZА-Я]+@[0-9a-zа-я_A-ZА-Я^.]+\.[a-zа-яА-ЯA-Z]{2,4}/i;
    if (!email_regexp.test($('#email').val())) {
        $('#email').addClass('error')
        $('#validEmail').text("Incorrect Email");
        emailFlag = false;
    } else {
        $('#email').removeClass('error');
        $('#email').addClass('not_error');
        $('#validEmail').text("");
        emailFlag = true;
    }
}

function checkName() {
    var name_regexp = /^[a-zA-Zа]+$/;
    var username = $('#username').val();
    if(username.length < 3) {
        $('#username').addClass('error');
        $('#validName').text("Name must be longer then 3 characters");
        nameFlag = false;
    }
    else if (!name_regexp.test(username)) {
        $('#username').addClass('error');
        $('#validName').text("Name must contains only [a-zA-Zа] and longer then 3 characters");
        nameFlag = false;
    } else {
        $('#username').removeClass('error');
        $('#username').addClass('not_error');
        $('#validName').text("");
        nameFlag = true;
    }
}

function checkPasswords() {
    var password1 = $('#password1').val();
    var password2 = $('#password2').val();
    if (password1.length < 6) {
        $('#password1').addClass('error')
        passwordFlag = false;
        $('#validPassword').text("Password must contains more then 6 characters");
    } else if (password1 != password2) {
        $('#password1').addClass('error');
        $('#password2').addClass('error');
        $('#validPassword').text("Passwords are not equal");
        passwordFlag = false;
    } else {
        $('#password1').removeClass('error');
        $('#password2').removeClass('error');
        $('#password1').addClass('not_error');
        $('#password2').addClass('not_error');
        $('#validPassword').text("");
        passwordFlag = true;
    }
}