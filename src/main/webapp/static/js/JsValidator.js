window.emailFlag = false;
window.nameFlag = false;
window.passwordFlag = false;
function finalValidate(form) {
    checkEmail();
    checkName();
    checkPasswords();
    checkFlags(form);
}
function checkFlags(form) {
    if (emailFlag == true && nameFlag == true && passwordFlag == true) {
        form.submit();
    }
}
function checkEmail() {
    var email_regexp = /[0-9a-zа-я_A-ZА-Я]+@[0-9a-zа-я_A-ZА-Я^.]+\.[a-zа-яА-ЯA-Z]{2,4}/i;
    var email = document.getElementById('email');
    if (!email_regexp.test(email.value)) {
        email.classList.add('error');
        document.getElementById('validEmail').textContent = "Email is invalid";
        emailFlag = false;
    } else {
        email.classList.remove('error');
        email.classList.add('not_error');
        document.getElementById('validEmail').textContent = "";
        emailFlag = true;
    }
}
function checkName() {
    var name_regex = /^[a-zA-Zа]+$/;
    var username = document.getElementById('username').value;
    if(username.length < 3) {
      document.getElementById('username').classList.add('error');
      nameFlag = false;
      document.getElementById('validName').textContent = "Name must be longer then 4 characters";
    }
     else if (!name_regex.test(username)) {
        document.getElementById('username').classList.add('error');
        document.getElementById('validName').textContent = "Name must contains only [a-zA-Zа] and longer then 3 characters";
        nameFlag = false;
    } else {
        document.getElementById('username').classList.remove('error');
         document.getElementById('username').classList.add('not_error');
        document.getElementById('validName').textContent ="";
        nameFlag = true;
    }
}
function checkPasswords() {
    var password1 = document.getElementById('password1').value;
    var password2 = document.getElementById('password2').value;
    if (password1.length < 6) {
       document.getElementById('password1').classList.add('error')
        passwordFlag = false;
        document.getElementById('validPassword').textContent = "Password must contains more then 6 characters";
    } else if (password1 != password2) {
        document.getElementById('password1').classList.add('error');
       document.getElementById('password2').classList.add('error');
       document.getElementById('validPassword').textContent("Passwords are not equal");
        passwordFlag = false;
    } else {
        document.getElementById('password1').classList.remove('error');
        document.getElementById('password2').classList.remove('error');
        document.getElementById('password1').classList.add('not_error');
        document.getElementById('password2').classList.add('not_error');
       document.getElementById('validPassword').textContent = "";
        passwordFlag = true;
    }
}