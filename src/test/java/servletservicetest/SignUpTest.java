package servletservicetest;
import strategy.captchastrategy.AbstractStrategy;
import strategy.captchastrategy.AttributeCaptchaStrategy;
import entity.Captcha;
import entity.User;
import org.junit.Assert;
import org.junit.Test;
import service.CaptchaService;
import service.ServiceManager;
import service.UserService;
import servlet.handleservlet.HandleSignUp;
import servlet.page.SignUpPage;
import util.CaptchaDrawer;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class SignUpTest {
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private RequestDispatcher requestDispatcher = mock(RequestDispatcher.class);
    private HttpSession httpSession = mock(HttpSession.class);
    private ServiceManager serviceManager = mock(ServiceManager.class);
    private ServletConfig servletConfig = mock(ServletConfig.class);
    private ServletContext servletContext = mock(ServletContext.class);
    private CaptchaService captchaService = mock(CaptchaService.class);
    private UserService userService = mock(UserService.class);
    private Map<String, Supplier<AbstractStrategy>> captchaStrategyMap = mock(LinkedHashMap.class);
    private User user = new User("username","login","lastname","password",
            "email", true,"filename");

    @Test
    public void CaptchaDrawerTest() {
        Assert.assertNotNull(CaptchaDrawer.drawCaptcha(new Captcha("123456")));
    }

    @Test
    public void SignUpServletShouldDoPost() throws ServletException, IOException {
        setMocksBehavior();
        SignUpPage signUpPage = new SignUpPage();
        signUpPage.init(servletConfig);
        when(signUpPage.getServletContext().getInitParameter("strategy")).thenReturn("attribute");
        signUpPage.doPost(request, response);
    }


    public void HandleSignUpShouldNotAcceptUserForm() throws ServletException, IOException {
        setMocksBehavior();
        HandleSignUp handleSignUp = new HandleSignUp();
        handleSignUp.init(servletConfig);
        when(userService.checkUserLogin(user)).thenReturn(false);
        handleSignUp.doPost(request, response);
        verify(request, atLeast(1)).setAttribute("email", user.getEmail());
        verify(request, atLeast(1)).setAttribute("username", user.getFirstName());
        verify(request, atLeast(1)).setAttribute("lastname", user.getLastName());
        verify(request, atLeast(1)).setAttribute("login", user.getLogin());
        verify(request, atLeast(1)).setAttribute("message", "message");
    }

    private void setMocksBehavior() throws IOException, ServletException {
        when(request.getRequestDispatcher("/WEB-INF/JSP/page-template.jsp")).thenReturn(requestDispatcher);
        when(request.getSession()).thenReturn(httpSession);
        when(servletConfig.getServletContext()).thenReturn(servletContext);
        when(servletContext.getAttribute("SERVICE_MANAGER")).thenReturn(serviceManager);
        when(servletContext.getInitParameter("strategy")).thenReturn("attribute");
        when(servletContext.getInitParameter("signUp-Timeout")).thenReturn("60000");
        when(captchaStrategyMap.get("attribute")).thenReturn(() -> new AttributeCaptchaStrategy());
        when(serviceManager.getCaptchaService()).thenReturn(captchaService);
        when(captchaService.getCaptchaStrategyMap()).thenReturn(captchaStrategyMap);
        when(userService.buildUserFromRequest(request)).thenReturn(user);
        when(serviceManager.getUserService()).thenReturn(userService);
        when(request.getRequestDispatcher("/signup")).thenReturn(requestDispatcher);
    }
}
