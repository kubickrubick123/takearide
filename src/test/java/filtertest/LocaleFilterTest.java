package filtertest;
import filter.LocaleFilter;
import org.junit.Assert;
import org.junit.Test;
import requestwrapper.LocaleHeaderRequestWrapper;
import strategy.captchastrategy.languagestoragestrategy.SessionStorageStrategy;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LocaleFilterTest {
    private FilterConfig filterConfig = mock(FilterConfig.class);
    private LocaleFilter localeFilter = new LocaleFilter();
    private HttpServletRequest request = mock(HttpServletRequest.class);
    private HttpServletResponse response = mock(HttpServletResponse.class);
    private HttpSession session = mock(HttpSession.class);
    private Locale ENGLISH_LOCALE = new Locale("en");
    private Locale SPANISH_LOCALE = new Locale("es");
    @Test
    public void shouldSetGivenLocalesFromDescriptor() throws ServletException, IOException {
        setMocksBehavior();
        LocaleHeaderRequestWrapper mutableRequest = new LocaleHeaderRequestWrapper(request, response,SPANISH_LOCALE,null,new SessionStorageStrategy());
        Assert.assertEquals(SPANISH_LOCALE,mutableRequest.getLocale());
    }
    @Test
    public void shouldSetNewLanguage() {
        setMocksBehavior();
        when(session.getAttribute("locale")).thenReturn(new Locale(""));
        LocaleHeaderRequestWrapper mutableRequest = new LocaleHeaderRequestWrapper(request, response,SPANISH_LOCALE,"en",new SessionStorageStrategy());
        Assert.assertEquals(ENGLISH_LOCALE,mutableRequest.getLocale());
    }

    public void setMocksBehavior() {
        List<Locale> userLocales = new ArrayList<>(Arrays.asList(SPANISH_LOCALE,ENGLISH_LOCALE));
        when(session.getAttribute("locale")).thenReturn(null);
        when(request.getSession()).thenReturn(session);
        when(filterConfig.getInitParameter("locales")).thenReturn("es");
        when(filterConfig.getInitParameter("languageStorageStrategy")).thenReturn("session");
        when(request.getLocales()).thenReturn(Collections.enumeration(userLocales));
    }
}
